import React from "react";
import {
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardText
} from 'reactstrap';

const Dishdetail = ({ dish }) => {

    if (!dish) return (<div></div>);

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <Card className="col-12">
                            <CardImg width={"100%"} src={dish.image} alt={dish.name} />
                            <CardBody>
                                <CardTitle>{dish.name}</CardTitle>
                                <CardText>{dish.description}</CardText>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-12 col-md-6">
                        <ul className="col-12 list-group">
                            {dish.comments.map(comment => {
                                
                                return (
                                    <li className="list-group-item" key={comment.id}>
                                        <div className="">
                                        { [...Array(comment.rating)].map((i, x) => {
                                            return (
                                                <svg key={x} width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 0L11.9095 4.99537L17.5595 6.21885L13.7077 10.5296L14.2901 16.2812L9 13.95L3.70993 16.2812L4.29227 10.5296L0.440492 6.21885L6.09046 4.99537L9 0Z" fill="#FEBB2C"/></svg>
                                            )
                                        }) }
                                        </div>
                                        <div><cite>{comment.comment}</cite></div>
                                        <small>{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</small>
                                        <p className="mb-0"><strong>{ comment.author }</strong></p>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Dishdetail;