import React, { Component } from 'react';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { DISHES } from '../shared/dishes';

import Home from '../Pages/Home';

import { Switch, Route } from 'react-router-dom';

class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
        dishes: DISHES,
    };
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route path="/" component={Home}></Route>
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default Main;